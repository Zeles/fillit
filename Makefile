NAME = fillit
SRCS = ./srcs/main.c ./srcs/error.c ./srcs/readfile.c ./srcs/createandresizefigure.c ./srcs/setpoint.c ./srcs/validfigure.c ./srcs/solution.c ./srcs/map.c
INCLUDES = includes/
BUILDFOLDER = ./build/

all: $(NAME)

debug: check_folder
	@$(MAKE) -C libft
	gcc -g -Wall -Wextra -Werror ${SRCS} ./libft/libft.a -I${INCLUDES} -o ${BUILDFOLDER}$(NAME)

$(NAME):
	@$(MAKE) -C libft
	gcc -Wall -Wextra -Werror ${SRCS} ./libft/libft.a -I${INCLUDES} -o $(NAME)

check_folder:
	if [ ! -d ${BUILDFOLDER} ]; then \
		mkdir ${BUILDFOLDER}; \
	fi

clean:
	@$(MAKE) -C libft clean
	rm -f *.o

fclean:
	@$(MAKE) -C libft fclean
	rm -f $(NAME)

re: fclean $(NAME)

re_d: fclean debug
